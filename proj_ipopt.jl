using Libdl
# Note: these filenames may differ. Check `/usr/lib/x86_64-linux-gnu` for the
# # specific extension.
Libdl.dlopen("/usr/lib/x86_64-linux-gnu/liblapack.so.3", RTLD_GLOBAL)
Libdl.dlopen("/usr/lib/x86_64-linux-gnu/libomp.so.5", RTLD_GLOBAL)


using JuMP
using Ipopt, Test


include("getData.jl")

#model = Model();
model = Model(Ipopt.Optimizer);

set_optimizer_attribute(model, "max_iter",  3000)
set_optimizer_attribute(model, "tol",  0.000001)
set_optimizer_attribute(model, "linear_solver", "pardiso")

n_T = 1

@variable(model, p[1:n_T, 1:n_G])




@expression(model, p_quad[t = 1:n_T], sum(p[t, i] * A[i, j] * p[t, j] for i in 1:n_G, j in 1:n_G) + sum(p[t, i] * b[i, 1] for i in 1:n_G) + c)
@constraint(model, balance[t = 1:n_T], 0 == p_quad[t])
@constraint(model, box[t = 1:n_T, g = 1:n_G], x_min[g, 1] <= p[t, g] <= x_max[g, 1])

@objective(model, Min, sum( (p[t, i]-x0[i, 1])^2 for i in 1:n_G, t in 1:n_T))

full_count = 0
function my_callback(
   prob::IpoptProblem,
   alg_mod::Cint,
   iter_count::Cint,
   obj_value::Float64,
   inf_pr::Float64,
   inf_du::Float64,
   mu::Float64,
   d_norm::Float64,
   regularization_size::Float64,
   alpha_du::Float64,
   alpha_pr::Float64,
   ls_trials::Cint,
)
   if full_count == 0
	global ipopt_time_0 = time()
   end
   global full_count = iter_count
   global ipopt_time = time()

	# return `true` to keep going, or `false` to terminate the optimization.
   return true
end
MOI.set(model, Ipopt.CallbackFunction(), my_callback)

total_time = @elapsed begin
	optimize!(model)
end
println("Total time:", total_time)
time2 = MOI.get(model, MOI.SolveTime())
println("Time2: ", time2)
ipopt_time2 = max(-ipopt_time_0 + ipopt_time, time2)
println("Time2: ", time2)
ipopt_time3 = -ipopt_time_0 + ipopt_time
println("Time3: ", ipopt_time3)
println(termination_status(model))

if termination_status(model) in [MOI.OPTIMAL, MOI.LOCALLY_SOLVED]
    optimal_solution = value.(p)
    optimal_objective = objective_value(model)
elseif termination_status(model) == MOI.TIME_LIMIT && has_values(model)
    suboptimal_solution = value.(p)
    suboptimal_objective = objective_value(model)
else
    error("The model was not solved correctly.")
end

println("x0:\n", x0)

println("Optimal solution: \n", optimal_solution)

dev = value.(p_quad[1])
iter = full_count

df_julia = DataFrame(obj=sqrt(optimal_objective), dev=dev, total_time=total_time, iter=iter, ipopt_time=ipopt_time2)



println(df)

CSV.write("tmp/julia/x_julia.csv", DataFrame(optimal_solution, :auto), header=false)
CSV.write("tmp/julia/df_julia.csv", df_julia)
