#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import random

#np.random.seed(42)

eps = 0.00001
dim = 10

x0 = np.random.randn(dim)
_A = np.random.randn(dim, dim)
A = _A + _A.T

c = np.random.randn(1)[0]

I = np.eye(dim)
E = np.ones(dim)

b = np.random.randn(dim)

eig, Q = np.linalg.eig(A)
L = eig

extrema = np.sort(-1/eig)


def inv_I_lA(l): return np.dot(np.dot(Q, np.diag(1/(E+l*L))), Q.T)


r1 = np.where(extrema < 0)[0][-1]
r2 = np.where(extrema > 0)[0][0]

r = 0

def x(l): return inv_I_lA(l) @ (x0 - l * b/2)

def f(l): return  x(l).T @ A @ x(l) + x(l).T @ b + c

def dist(l): return None

fig, ax = plt.subplots(2, 2, sharex=True)

T = np.linspace(-1, 1, 1000)
y1 = np.zeros_like(T)
y2 = np.zeros_like(T)
y3 = np.zeros_like(T)
for i, t in enumerate(T):
    y2[i] = np.sum(x(t))
    y1[i] = f(t)
    y3[i] = np.linalg.norm(x(t) - x0)

y4 = np.sign(y1)

fig.tight_layout()
ax[1, 0].scatter(extrema, extrema*0, color='red')
ax[0, 1].set_title(r'$f(\lambda)$')
ax[0, 1].plot(T, y1)
ax[0, 1].scatter(extrema, extrema*0, color='red')
ax[0, 0].set_title(r'$\sum_i x_i$')
ax[0, 0].scatter(extrema, extrema*0, color='red')
ax[0, 0].plot(T, y2)
ax[1, 0].set_title(r'$|| x(\lambda) - x_0 ||_2$')
ax[1, 0].plot(T, y3)
ax[1, 1].set_title(r'$sign(f(\lambda))$')
ax[1, 1].set_xlim([-1, 1])
ax[1, 1].scatter(extrema, extrema*0-1, color='red')
ax[1, 1].plot(T, y4)
ax[1, 1].set_xlabel(r'$\lambda$')
ax[1, 0].set_xlabel(r'$\lambda$')

plt.show()
