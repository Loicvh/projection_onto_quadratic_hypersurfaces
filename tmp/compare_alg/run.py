
from cycler import cycler
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.font_manager
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import pickle
#plt.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
plt.rcParams.update({
    "text.usetex": True,
   # "font.serif": ["Palatino"],
   # "font.serif": ["Times"],
 #   "font.family": "sans-serif",
 #   "font.sans-serif": ["Helvetica"],
 #   'font.family' : 'lmodern',
    "font.size": 18,
})

CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

custom_cycler = (cycler(color=CB_color_cycle))
alpha = 0.5
data = pickle.load(open('comparison.pickle', 'rb'))

legend_position = 'upper right'
q_label = ['timeout', 'iteration', 'distance-rel']
q_zoom = ['distance']
dim_zoom = ['alternate-projection', 'douglas-rachford', 'IPOPT']

flag_scatter = True

eps_log = pow(10, -6)

#label_to_drop = [('alternate-projection', 'quasi')]
label_to_drop = []
for _d in data:
    for lab in label_to_drop:
        data[_d].drop([lab], axis=1, inplace=True)


def get_label(dim):
    if dim[0] == "IPOPT":
        return "IPOPT", CB_color_cycle[0], 'o'
    elif dim[0] == "douglas-rachford":
        return "DR", CB_color_cycle[1], '1'
    elif dim[0] == "douglas-rachford-feasibility":
        return "DR-F", CB_color_cycle[2], '4'
    elif dim[0] == "dykstra":
        return "dykstra", CB_color_cycle[3], '3'
    elif dim[0] == "alternate-projection":
        if dim[1] == "exact":
            return "APE", CB_color_cycle[4], 's'
        elif dim[1] == "quasi":
            return "APC", CB_color_cycle[5], '1'
        elif dim[1] == "quasi-dir":
            return "APD", CB_color_cycle[7], 's'
        elif dim[1] == "current-grad":
            return "APG", CB_color_cycle[6], '+'

for d in data['timeout']:
    print(data['timeout'][d])
data['mean-distance-rel'] = {}
for d in data['mean-distance']:
    data['mean-distance-rel'][d] = data['mean-distance'][d]
    data['mean-distance-rel'][d] = data['mean-distance'][d]/data['mean-distance'][('alternate-projection', 'exact')]*100-100+eps_log
print(data['mean-distance-rel'])

eps_p = pow(10, -16)
data['min-deviation'] = data['min-deviation'] + eps_p

#print(data['timeout'][('alternate-projection', 'quasi')])
#print(data['min-deviation'][('alternate-projection', 'quasi-dir')])
#print(data['mean-distance'][[('IPOPT', 'exact'), ('alternate-projection', 'exact')]])
for q in ['distance', 'iteration']: 
    min_q = "min-"+q
    max_q = "max-"+q
    mean_q = "mean-"+q
    fig, ax = plt.subplots()
    if q in q_zoom:

        axins = zoomed_inset_axes(ax, 20, loc=2) # 2
        axins.set_autoscale_on(False)
        #_, axins = plt.subplots()
        
    _min = float('inf')
    _max = - float('inf')
    for i, dim in enumerate(data[mean_q].columns):
        label, color, marker = get_label(dim)
        color_rgb = colors.to_rgba(color)
        color_alpha = list(color_rgb)
        color_alpha[3] = alpha
        ax.plot(data[mean_q][dim], color=color, zorder=1)
        ax.plot(data[max_q][dim], linestyle='dashed', color=color_alpha, zorder=1)
        ax.plot(data[min_q][dim], linestyle='dashed', color=color_alpha, zorder=1)
        if flag_scatter:
            color_alpha[-1] = 0.5
            ax.scatter(data[mean_q][dim].index, data[mean_q][dim], color=color_alpha, marker=marker, zorder=2, label=label)
            ax.scatter(data[max_q][dim].index, data[max_q][dim], color=color_alpha, marker=marker, zorder=2, facecolor=color_alpha, edgecolor=color_alpha)
            ax.scatter(data[min_q][dim].index, data[min_q][dim], color=color_alpha, zorder=2, marker=marker, facecolor=color_alpha, edgecolor=color_alpha)
        print('dim', dim)

        if q in q_zoom and dim[0] in dim_zoom and dim[1] not in ['quasi']:

            print('dim', dim)
            _dim = data[mean_q].index[0] 
            _dim_shift = 1
            _dim_shift = (data[mean_q].index[1]- _dim )/100
            axins.set_xlim(_dim -_dim_shift, _dim +_dim_shift)
            _dat = data[mean_q][dim][_dim]
            _max = max(_max, _dat)
            _min = min(_min, _dat)
            dm = (_max - _min)
            axins.set_ylim(_min-dm/10, _max+dm/10)
            axins.set_xticks([])
            axins.yaxis.set_major_locator(plt.MaxNLocator(2))
            axins.yaxis.set_ticks_position("right")
            #axins.set_yticks([])
            mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5", lw=0.5)
            #axins.set_ylim(1, 2)
            if dim[0] in ['IPOPT', 'APE']:
                color = color_alpha
            axins.scatter(data[mean_q][dim].index, data[mean_q][dim], color=color, zorder=2, marker=marker)
        i += 1
    if q in q_label:
        ax.legend(loc=legend_position)
    #ax.set_ylabel(r'%s' % q)
    ax.set_ylabel(q)
    ax.set_xlabel(r'$n$')
    ax.yaxis.set_label_position("right")
    #print(data['mean-distance'][('IPOPT', 'exact')])
    #plt.show()
    plt.subplots_adjust(bottom=0.15)
    fig.savefig(q+str(".png"))
    fig.savefig(q+str(".pdf"))
for q in ['deviation', 'total-time']:
    min_q = "min-"+q
    max_q = "max-"+q
    mean_q = "mean-"+q
    fig, ax = plt.subplots()
    i = 0
    ylabel = q
    if q in ['total-time']: # 'time'
        ylabel = r'time (s)'
    for i, dim in enumerate(data[mean_q].columns):
        label, color, marker = get_label(dim)
        ax.semilogy(data[mean_q][dim], color=color)
        ax.semilogy(data[max_q][dim], linestyle='dashed', color=color, alpha=alpha)
        ax.semilogy(data[min_q][dim], linestyle='dashed', color=color, alpha=alpha)
        i += 1
        if flag_scatter:
            ax.scatter(data[mean_q][dim].index, data[mean_q][dim], marker=marker, color=color, alpha=alpha, label=label)
            ax.scatter(data[max_q][dim].index, data[max_q][dim], marker=marker, color=color, alpha=alpha)
            ax.scatter(data[min_q][dim].index, data[min_q][dim], marker=marker, color=color, alpha=alpha)
    if q in q_label:
        ax.legend(loc=legend_position)
    ax.set_ylabel(r'%s' % ylabel)
    ax.set_xlabel(r'$n$')
    plt.subplots_adjust(bottom=0.15)
    ax.yaxis.set_label_position("right")
    fig.savefig(q+str(".png"))
    fig.savefig(q+str(".pdf"))

for q in ['distance-rel']:
    mean_q = "mean-"+q
    fig, ax = plt.subplots()
    i = 0
    ylabel = q
    if q in ['total-time']: # 'time'
        ylabel = r'time (s)'
    if q in ['distance-rel']: # 'time'
        ylabel = r'Relative d'
    for i, dim in enumerate(data[mean_q]):
        print(max(data[mean_q][dim]), eps_p)
        if max(data[mean_q][dim]) > eps_log*2:
            label, color, marker = get_label(dim)
            ax.semilogy(data[mean_q][dim], color=color)
            i += 1
            if flag_scatter:
                ax.scatter(data[mean_q][dim].index, data[mean_q][dim], marker=marker, color=color, alpha=alpha)
        if q in q_label:
            ax.legend(loc=legend_position)
        ax.set_ylabel(r'%s' % ylabel)
        ax.set_xlabel(r'$n$')
        ax.yaxis.set_label_position("right")
        plt.subplots_adjust(bottom=0.15)
        fig.savefig(q+str(".png"))
        fig.savefig(q+str(".pdf"))


fig, ax = plt.subplots()
for i, dim in enumerate(data['timeout'].columns):
    label, color, marker = get_label(dim)
    ax.plot(data['timeout'][dim], color=color)
    ax.scatter(data['timeout'][dim].index, data['timeout'][dim], color=color, marker=marker, label=label)

q = 'timeout'
if q in q_label:
    ax.legend(loc=legend_position)
ax.set_ylabel(r'%s' % q)
ax.set_xlabel(r'$n$')
ax.yaxis.set_label_position("right")
plt.subplots_adjust(bottom=0.15)
fig.savefig('timeout.png')
fig.savefig('timeout.pdf')
#plt.show()
#print(data['mean-distance'][('IPOPT', 'exact')])
#print(data['mean-distance'][('alternate-projection', 'exact')])
