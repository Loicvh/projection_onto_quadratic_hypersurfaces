#!/usr/bin/env sh

if [[ $# -eq 0 ]] ; then
	echo 'pass the n in argument (n=100 or 1000) and ellipsoid or hyperboloid'
    exit 0
fi
N=$1
TYPE=$2

for file in *.pdf
do
	echo $file
	new_file="/home/loicvh/Documents/overleaf/Alternate-projection-quadric/images/${file%%.*}_${TYPE}_${N}.pdf"
	echo "copying ${file} into ${new_file}"
	cp $file $new_file
done
