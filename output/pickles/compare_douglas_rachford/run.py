
import matplotlib.pyplot as plt
import pickle

data = pickle.load(open('comparison.pickle', 'rb'))
print(data['df_time'].to_string())
data['df_distance'].plot()
plt.ylabel('distance')
plt.savefig('/home/loicvh/Documents/overleaf/Alternate-projection-quadric/images/comparison_distance_100_dim_log.pdf')
data['df_time'].plot()
plt.ylabel('time')
plt.savefig('/home/loicvh/Documents/overleaf/Alternate-projection-quadric/images/comparison_time_100_dim_log.pdf')
#plt.show()
print(data['df_timeout'][('IPOPT', 'exact')])
print(data['df_timeout'][('alternate-projection', 'quasi')])
print(data)
