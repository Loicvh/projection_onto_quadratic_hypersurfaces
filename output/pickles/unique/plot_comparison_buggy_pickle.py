from cycler import cycler
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import matplotlib as mpl

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.serif": ["Palatino"],
})

CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

custom_cycler = (cycler(color=CB_color_cycle))

dir_str = '.'


list_keys = ['err_alt', 'err_exact', 'time_alt', 'time_rel', 'rel_err']
list_keys_new = ['err_alt', 'err_exact', 'time_alt', 'time_rel', 'rel_err', 'time_exact', 'timeout']


data = {key: {} for key in list_keys_new}
data['time_exact'] = {}

for filename in os.listdir(dir_str):
    if filename.endswith(".pickle"):
        split = filename.split('.pickle')[0].split('_')
        dim_s = split[-2]
        dim_e = split[-1]

        print(dim_s, dim_e)
        str_file = os.path.join(dir_str, filename)
        with open(str_file, 'rb') as f:
            dim_data = pickle.load(f)
        print(dim_data.keys())
        for dim in range(int(dim_s), int(dim_e)):
            for key in list_keys:
                data[key][dim] = np.mean(dim_data[key][dim])
            data['time_exact'][dim] = np.mean((np.power(np.e, dim_data['time_rel'][dim]) -1)*dim_data['time_alt'][dim] + dim_data['time_alt'][dim])
            data['timeout'][dim] = np.sum(np.isinf(dim_data['err_exact'][dim]))

dim_s = min(data[list_keys[0]].keys())
dim_e = max(data[list_keys[0]].keys())


dims = np.arange(dim_s, dim_e+1)
new_data = {key: np.zeros(dim_e-dim_s+1) for key in list_keys_new}
for key in list_keys_new:
    for dim in dims:
        new_data[key][dim-dim_s] = data[key][dim]

max_no_inf = np.max(new_data['err_exact'][np.isfinite(new_data['err_exact'])])
new_data['err_exact'][np.where(np.isinf(new_data['err_exact']))] = max_no_inf

new_data['time_rel'] = new_data['time_rel'] / np.log(10)

new_data['time_rel'] = new_data['time_alt'] / new_data['time_exact']
new_data['rel_err'] = new_data['err_alt'] / new_data['err_exact']
print(new_data)


fig, ax1 = plt.subplots()

#ax1.set_title('Test')


ax2 = ax1.twinx()
ax3 = ax1.twinx()
color_err = CB_color_cycle[0]
color_time = CB_color_cycle[1]
ax1.semilogy(dims, new_data['rel_err'] , color=color_err, label='Rel error')
ax2.semilogy(dims, new_data['time_rel'], color=color_time, label='Time', linestyle='dashed')
ax1.set_xlabel(r'\textbf{Dimension}')
ax1.set_ylabel(r'\textbf{Relative distance}', color=color_err)
ax2.set_ylabel(r'\textbf{Relative time}', color=color_time)
#ax2.set_ylabel(r'mean(log( 1 + (time_ex - time_alt)+time_alt))', color=color_time)
bar = ax3.bar(dims, new_data['timeout'], color=color_err, zorder=2, alpha=0.3, label='Timeout of Gurobi')


ax3.set_yticks([])
ax3.legend(loc='lower left')
ax3.set
height = 0
for rect in bar:
    prev_height = height
    height = rect.get_height()
    if height > 0 and prev_height != height:
        ax3.text(rect.get_x() + rect.get_width()/2.0, height, '%d' % int(height), ha='center', va='bottom')



fig.savefig('Comparison.pdf')


