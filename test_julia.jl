using JuMP, Ipopt, Test
model = Model(Ipopt.Optimizer)
@variable(model, x >= 1)
@objective(model, Min, x + 0.5)

full_count = 0
function my_callback(
   prob::IpoptProblem,
   alg_mod::Cint,
   iter_count::Cint,
   obj_value::Float64,
   inf_pr::Float64,
   inf_du::Float64,
   mu::Float64,
   d_norm::Float64,
   regularization_size::Float64,
   alpha_du::Float64,
   alpha_pr::Float64,
   ls_trials::Cint,
)
   global full_count = iter_count
	# return `true` to keep going, or `false` to terminate the optimization.
   return true
end
MOI.set(model, Ipopt.CallbackFunction(), my_callback)
optimize!(model)
println("Iteration = ", full_count)
