using Base: Filesystem.isfile
using CSV
using DataFrames

file_path="data/data_ipopt/"
println(file_path)

function load_CSV(file_name::AbstractString, remove_first::Bool=false)
	rel_path = string(file_path, file_name)
	df = DataFrame(CSV.File(rel_path; header=false))
	if remove_first
		df = select(df, Not(:Column1))
	end
	return df
end

""" x0 """
x0 = load_CSV("x0.csv")
rename!(x0, ["x0"])
println("x0", x0)
df = DataFrame()
df[!, "x0"] = [x0]

n_G = nrow(x0)

println("Number of generators is ", n_G)


""" A """


A = load_CSV("A.csv")
#println(A)
#println("Column A[1, : ] = ", A[1, :Column1])

""" b """

b = load_CSV("b.csv")

c = load_CSV("c.csv")
c = c[1, 1]

""" x_max """

x_max = load_CSV("x_max.csv")
""" x_min """

x_min = load_CSV("x_min.csv")

